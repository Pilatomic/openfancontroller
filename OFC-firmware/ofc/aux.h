#ifndef AUX_H
#define AUX_H

#include <stdint.h>

void aux_ws2812_setup(void);
void aux_ws2812_write(uint32_t offset, uint32_t size, uint8_t *data);

#endif
