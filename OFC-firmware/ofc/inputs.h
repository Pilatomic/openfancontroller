#ifndef INPUTS_H
#define INPUTS_H

#include <stdint.h>

#define INPUTS_COUNT          4

enum inputs_enum { in1 = 0, in2, in3, in4 };


void inputs_setup(void);

void inputs_startConversions(void);

int inputs_isConversionsFinished(void);

uint16_t inputs_getValue(enum inputs_enum input);

#endif
