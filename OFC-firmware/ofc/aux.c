#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include "aux.h"

#define AUX_TIMER       TIM1
#define AUX_TIMER_RCC   RCC_TIM1
#define AUX_TIMER_RST   RST_TIM1
#define AUX_PORT        GPIOA
#define AUX_PORT_RCC    RCC_GPIOA
#define AUX_PIN_1       GPIO8
#define AUX_PIN_2       GPIO9
#define AUX_PIN_3       GPIO10
#define AUX_OC_1        TIM_OC1
#define AUX_OC_2        TIM_OC2
#define AUX_OC_3        TIM_OC3
#define AUX_CCR_1       (TIM_CCR1(AUX_TIMER))
#define AUX_CCR_2       (TIM_CCR2(AUX_TIMER))
#define AUX_CCR_3       (TIM_CCR3(AUX_TIMER))
#define AUX_DMA         DMA1
#define AUX_DMA_CHANNEL DMA_CHANNEL5
#define AUX_DMA_RCC     RCC_DMA1

#define WS2812_TIMER_PRESCALER  5
#define WS2812_TIMER_PERIOD     9
#define WS2812_CCVAL_0          2
#define WS2812_CCVAL_1          7

// Lot of help from this code : https://github.com/Daedaluz/stm32-ws2812/blob/master/src/ws2812.c

#define BUFFER_RESET_SIZE  80
#define BUFFER_TOTAL_SIZE (3*24 + BUFFER_RESET_SIZE)
static uint16_t write_buffer[BUFFER_TOTAL_SIZE];

void aux_ws2812_write(uint32_t offset, uint32_t size, uint8_t* data)
{
    uint32_t x = 0;
    for(x = 0; x < size; x++) {
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+0] = (data[x] & 0x80) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+1] = (data[x] & 0x40) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+2] = (data[x] & 0x20) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+3] = (data[x] & 0x10) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+4] = (data[x] & 0x08) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+5] = (data[x] & 0x04) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+6] = (data[x] & 0x02) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
        write_buffer[BUFFER_RESET_SIZE+((x+offset)*8)+7] = (data[x] & 0x01) ? WS2812_CCVAL_1:WS2812_CCVAL_0;
    }
}


void aux_ws2812_setup(void)
{
    //BLANK BUFFERs
    for(int i = 0 ; i < BUFFER_RESET_SIZE  ; i++)
    {
        write_buffer[i] = 0;
    }
    for(int i = BUFFER_RESET_SIZE ; i < BUFFER_TOTAL_SIZE ; i++)
    {
        write_buffer[i] = WS2812_CCVAL_0;
    }

    //aux_ws2812_write(3, 3, "\x0F\x0F\x0F");

    rcc_periph_clock_enable(AUX_PORT_RCC);
    rcc_periph_clock_enable(AUX_TIMER_RCC);
    rcc_periph_reset_pulse(AUX_TIMER_RST);

    timer_set_mode(AUX_TIMER, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_disable_preload(AUX_TIMER);
    timer_continuous_mode(AUX_TIMER);
    timer_set_prescaler(AUX_TIMER, WS2812_TIMER_PRESCALER);
    timer_set_period(AUX_TIMER, WS2812_TIMER_PERIOD);
    timer_enable_break_main_output(AUX_TIMER);

    //Only implement aux pin 0 for now
    gpio_mode_setup(AUX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, AUX_PIN_1);
    gpio_set_af(AUX_PORT, GPIO_AF2, AUX_PIN_1);
    gpio_set_output_options(AUX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, AUX_PIN_1);
#warning check if it works with 25MHz speed

    timer_disable_oc_output(AUX_TIMER, AUX_OC_1);
    timer_set_oc_mode(AUX_TIMER, AUX_OC_1, TIM_OCM_PWM1);
    timer_set_oc_value(AUX_TIMER, AUX_OC_1, WS2812_CCVAL_0);
#warning value of 4 for debug only, set back to 0
    timer_set_oc_polarity_low(AUX_TIMER, AUX_OC_1);    // Invert polarity, due to inverted level shifter in front pcb
    timer_enable_oc_preload(AUX_TIMER, AUX_OC_1);
    timer_enable_oc_output(AUX_TIMER, AUX_OC_1);


    rcc_periph_clock_enable(AUX_DMA_RCC);

    dma_channel_reset(AUX_DMA, AUX_DMA_CHANNEL);

    dma_set_peripheral_address(AUX_DMA, AUX_DMA_CHANNEL, (uint32_t)(&AUX_CCR_1));
    dma_set_memory_address(AUX_DMA, AUX_DMA_CHANNEL, (uint32_t)(&write_buffer[0]));
    dma_set_number_of_data(AUX_DMA, AUX_DMA_CHANNEL, BUFFER_TOTAL_SIZE);
    dma_set_read_from_memory(AUX_DMA, AUX_DMA_CHANNEL);
    dma_enable_memory_increment_mode(AUX_DMA, AUX_DMA_CHANNEL);
    dma_set_peripheral_size(AUX_DMA, AUX_DMA_CHANNEL, DMA_CCR_PSIZE_16BIT);
    dma_set_memory_size(AUX_DMA, AUX_DMA_CHANNEL, DMA_CCR_MSIZE_16BIT);
    dma_enable_circular_mode(AUX_DMA, AUX_DMA_CHANNEL);
    dma_set_priority(AUX_DMA, AUX_DMA_CHANNEL, DMA_CCR_PL_MEDIUM);

    //dma_enable_transfer_complete_interrupt(AUX_DMA, AUX_DMA_CHANNEL);

    timer_set_dma_on_update_event(AUX_TIMER);
    timer_enable_irq(AUX_TIMER, TIM_DIER_UDE);

    dma_enable_channel(AUX_DMA, AUX_DMA_CHANNEL);
    timer_enable_counter(AUX_TIMER);
}
