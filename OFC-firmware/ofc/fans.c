#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

#include "fans.h"

#define FANS_PWM_TIMER      TIM2
#define FANS_PWM_TIMER_RCC  RCC_TIM2
#define FANS_PWM_TIMER_RST  RST_TIM2
#define FANS_PWM_PORT       GPIOA
#define FANS_PWM_PORT_RCC   RCC_GPIOA

#define FANS_TACH_TIMER     TIM3
#define FANS_TACH_TIMER_RCC RCC_TIM3
#define FANS_TACH_TIMER_RST RST_TIM3
#define FANS_TACH_TIMER_IRQ NVIC_TIM3_IRQ
#define FANS_TACH_TIMER_ISR tim3_isr
#define FANS_TACH_PORT      GPIOB
#define FANS_TACH_PORT_RCC  RCC_GPIOB

#define FANS_TACH_RPM_PER_PULSE 30

// Values below are based on a 48MHz oscillator
#define FANS_PWM_PERIOD     254     // So that 255 is full speed
#define FANS_PWM_PRESCALER  7       // Gives 23.5 kHz, close enough to 25 kHz.

#define FANS_TACH_PRESCALER 732     // Should give 1HZ OF interrupt

#warning CURRENTLY USE A CRUDE TICK COUNT TO GET RPM. MAYBE RATHER MEASURE DELAY BETWEEN 4 PULSES OR SOMETHING LIKE THAT TO GET BETTER RPM PRECISION ?


struct fans_info_t
{
    uint16_t            pwm_pin;
    enum tim_oc_id      pwm_oc_id;
    uint16_t            tach_pin;
    enum tim_ic_id      tach_ic_id;
    enum tim_ic_input   tach_ic_input;
    int                 tach_irq_bit;
    int                 tach_irq_flag;
};

static const struct fans_info_t fans_info[FANS_COUNT] =
{
    { GPIO3     , TIM_OC4   , GPIO1     , TIM_IC4   , TIM_IC_IN_TI4 , TIM_DIER_CC4IE    , TIM_SR_CC4IF  },
    { GPIO2     , TIM_OC3   , GPIO0     , TIM_IC3   , TIM_IC_IN_TI3 , TIM_DIER_CC3IE    , TIM_SR_CC3IF  },
    { GPIO1     , TIM_OC2   , GPIO5     , TIM_IC2   , TIM_IC_IN_TI2 , TIM_DIER_CC2IE    , TIM_SR_CC2IF  },
    { GPIO0     , TIM_OC1   , GPIO4     , TIM_IC1   , TIM_IC_IN_TI1 , TIM_DIER_CC1IE    , TIM_SR_CC1IF  },
};


static void fans_setup_pwm(void)
{
    rcc_periph_clock_enable(FANS_PWM_PORT_RCC);
    rcc_periph_clock_enable(FANS_PWM_TIMER_RCC);
    rcc_periph_reset_pulse(FANS_PWM_TIMER_RST);

    timer_set_mode(FANS_PWM_TIMER, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_disable_preload(FANS_PWM_TIMER);
    timer_continuous_mode(FANS_PWM_TIMER);
    timer_set_prescaler(FANS_PWM_TIMER, FANS_PWM_PRESCALER);
    timer_set_period(FANS_PWM_TIMER, FANS_PWM_PERIOD);
    timer_enable_break_main_output(FANS_PWM_TIMER);


    for(int i = 0 ; i < FANS_COUNT ; i++)
    {
        gpio_mode_setup(FANS_PWM_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, fans_info[i].pwm_pin);
        gpio_set_af(FANS_PWM_PORT, GPIO_AF2, fans_info[i].pwm_pin);
        gpio_set_output_options(FANS_PWM_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, fans_info[i].pwm_pin);

        timer_disable_oc_output(FANS_PWM_TIMER, fans_info[i].pwm_oc_id);
        timer_set_oc_mode(FANS_PWM_TIMER, fans_info[i].pwm_oc_id, TIM_OCM_PWM2);
        timer_set_oc_value(FANS_PWM_TIMER, fans_info[i].pwm_oc_id, FANS_PWM_PERIOD+1);
        timer_enable_oc_preload(FANS_PWM_TIMER, fans_info[i].pwm_oc_id);
#warning just added the previous line, need to check it works !!!
        timer_enable_oc_output(FANS_PWM_TIMER, fans_info[i].pwm_oc_id);
    }

    timer_enable_counter(FANS_PWM_TIMER);
}

static void fans_setup_tach(void)
{
    rcc_periph_clock_enable(FANS_TACH_PORT_RCC);
    rcc_periph_clock_enable(FANS_TACH_TIMER_RCC);
    rcc_periph_reset_pulse(FANS_TACH_TIMER_RST);

    timer_set_mode(FANS_TACH_TIMER, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    timer_disable_preload(FANS_TACH_TIMER);
    timer_one_shot_mode(FANS_TACH_TIMER);
    //timer_continuous_mode(FANS_TACH_TIMER);
    timer_update_on_overflow(FANS_TACH_TIMER);
    timer_set_prescaler(FANS_TACH_TIMER, FANS_TACH_PRESCALER);
    //timer_set_period(FANS_TACH_TIMER, 65535);  // Full timer period
    timer_enable_break_main_output(FANS_TACH_TIMER);

    nvic_enable_irq(FANS_TACH_TIMER_IRQ);
    nvic_set_priority(FANS_TACH_TIMER_IRQ,0);
    timer_enable_irq(FANS_TACH_TIMER, TIM_DIER_UIE);

    for(int i = 0 ; i < FANS_COUNT ; i++)
    {
        gpio_mode_setup(FANS_TACH_PORT, GPIO_MODE_AF, GPIO_PUPD_PULLUP, fans_info[i].tach_pin);
        gpio_set_af(FANS_TACH_PORT, GPIO_AF1, fans_info[i].tach_pin);

        timer_ic_disable(FANS_TACH_TIMER, fans_info[i].tach_ic_id);
        timer_ic_set_input(FANS_TACH_TIMER, fans_info[i].tach_ic_id, fans_info[i].tach_ic_input);
        timer_ic_set_filter(FANS_TACH_TIMER, fans_info[i].tach_ic_id, TIM_IC_DTF_DIV_32_N_8);       // Enable some filtering on input
        timer_ic_set_polarity(FANS_TACH_TIMER, fans_info[i].tach_ic_id, TIM_IC_RISING);             // Capture falling edge at fan connector
        timer_ic_set_prescaler(FANS_TACH_TIMER, fans_info[i].tach_ic_id, TIM_IC_PSC_OFF);           // No capture prescaler
        timer_enable_irq(FANS_TACH_TIMER, fans_info[i].tach_irq_bit);
        timer_ic_enable(FANS_TACH_TIMER, fans_info[i].tach_ic_id);
    }

    timer_enable_counter(FANS_TACH_TIMER);

}

static volatile int tachPulsesCount[FANS_COUNT];
static volatile int tachRpm[FANS_COUNT];

void FANS_TACH_TIMER_ISR(void)
{
    if(timer_get_flag(FANS_TACH_TIMER, TIM_SR_UIF))
    {
        nvic_disable_irq(FANS_TACH_TIMER_IRQ);
    }

    for(int i = 0 ; i < FANS_COUNT ; i++)
    {
        int flagForCurrentFan = fans_info[i].tach_irq_flag;
        if(timer_get_flag(FANS_TACH_TIMER, flagForCurrentFan))
        {
            timer_clear_flag(FANS_TACH_TIMER, flagForCurrentFan);
            tachPulsesCount[i]++;
        }
    }
}

void fans_setup(void)
{
    for(int i = 0 ; i < FANS_COUNT ; i++)
    {
        tachPulsesCount[i] = 0;
        tachRpm[i] = 0;
    }

    fans_setup_pwm();
    fans_setup_tach();

}

void fans_setFanPwm(enum fans_enum fan, uint8_t value)
{
    timer_set_oc_value(FANS_PWM_TIMER, fans_info[fan].pwm_oc_id, value);
}

void fans_startRpmCount(void)
{
    // Clear interrupt flags, pulses count, and reenable interrupts
    timer_clear_flag(FANS_TACH_TIMER, TIM_SR_UIF);
    for(int i = 0 ; i < FANS_COUNT ; i++)
    {
        timer_clear_flag(FANS_TACH_TIMER, fans_info[i].tach_irq_flag);
        tachPulsesCount[i] = 0;
    }
    timer_enable_counter(FANS_TACH_TIMER);
    nvic_enable_irq(FANS_TACH_TIMER_IRQ);
}

int fans_isRpmCountFinished(void)
{
    return !nvic_get_irq_enabled(FANS_TACH_TIMER_IRQ);
}

unsigned int fans_getRpmCountForFan(enum fans_enum fan)
{
    return tachPulsesCount[fan] * FANS_TACH_RPM_PER_PULSE ;
}
