#ifndef FANS_H
#define FANS_H

#include <stdint.h>

#define FANS_COUNT          4

enum fans_enum { fan1 = 0, fan2, fan3, fan4 };
void fans_setup(void);
void fans_setFanPwm(enum fans_enum fan, uint8_t value);

void fans_startRpmCount(void);
int fans_isRpmCountFinished(void);
unsigned int fans_getRpmCountForFan(enum fans_enum fan);

#endif
