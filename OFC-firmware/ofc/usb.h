#ifndef USB_H
#define USB_H

void usb_setup(void);
void usb_poll(void);
void usb_send_report(char* buf, int length);
#endif
