#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include <string.h>

#include "inputs.h"

#define GPIO_ANALOG_INPUTS  (GPIO4 | GPIO5 | GPIO6 | GPIO7)
#define ADC_NUM             ADC1
#define ADC_DMA_CHANNEL     DMA_CHANNEL1
#define ADC_DMA             DMA1
#define ADC_DMA_RCC         RCC_DMA1
#define CHANNEL_COUNT       5

static const uint8_t channel_array[CHANNEL_COUNT] = { 4, 5, 6, 7, ADC_CHANNEL_TEMP};

static uint16_t conv_results[CHANNEL_COUNT];

#warning REMEMBER TO EVALUTE USE OF INTERNAL TEMPERATURE SENSOR

void inputs_setup(void)
{
    rcc_periph_clock_enable(RCC_ADC);
    rcc_periph_clock_enable(RCC_GPIOA);

    gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_ANALOG_INPUTS);

    adc_power_off(ADC_NUM);
    adc_set_clk_source(ADC_NUM, ADC_CLKSOURCE_ADC); // Use dedicated 14MHz ADC clock
    adc_calibrate(ADC_NUM);
    adc_set_operation_mode(ADC_NUM, ADC_MODE_SCAN);
    adc_disable_external_trigger_regular(ADC_NUM);
    adc_set_right_aligned(ADC_NUM);
    adc_enable_temperature_sensor();
    adc_set_sample_time_on_all_channels(ADC_NUM, ADC_SMPTIME_071DOT5);
    adc_set_regular_sequence(ADC_NUM, CHANNEL_COUNT, (uint8_t*)channel_array);
    adc_set_resolution(ADC_NUM, ADC_RESOLUTION_12BIT);
    adc_disable_analog_watchdog(ADC_NUM);
    adc_power_on(ADC_NUM);

#warning ADC STARTUP WAIT Should not be needed
    /* Wait for ADC starting up. */
    int i;
    for (i = 0; i < 800000; i++) {    /* Wait a bit. */
        __asm__("nop");
    }

    rcc_periph_clock_enable(ADC_DMA_RCC);

    dma_channel_reset(ADC_DMA, ADC_DMA_CHANNEL);

    dma_set_peripheral_address(ADC_DMA, ADC_DMA_CHANNEL, (uint32_t)(&ADC1_DR));
    dma_set_memory_address(ADC_DMA, ADC_DMA_CHANNEL, (uint32_t)(&conv_results[0]));
    dma_set_number_of_data(ADC_DMA, ADC_DMA_CHANNEL, CHANNEL_COUNT);
    dma_set_read_from_peripheral(ADC_DMA, ADC_DMA_CHANNEL);
    dma_enable_memory_increment_mode(ADC_DMA, ADC_DMA_CHANNEL);
    dma_enable_circular_mode(ADC_DMA, ADC_DMA_CHANNEL); //Else DMA stops after first transfer
    dma_set_peripheral_size(ADC_DMA, ADC_DMA_CHANNEL, DMA_CCR_PSIZE_16BIT);
    dma_set_memory_size(ADC_DMA, ADC_DMA_CHANNEL, DMA_CCR_MSIZE_16BIT);
    dma_set_priority(ADC_DMA, ADC_DMA_CHANNEL, DMA_CCR_PL_HIGH);

    dma_enable_channel(ADC_DMA, ADC_DMA_CHANNEL);

    adc_enable_dma(ADC_NUM);

#warning possibly missing stuff here ( set dma on adc event maybe ? not sure )
    memset(conv_results, 0x00, sizeof(conv_results));
}

void inputs_startConversions(void)
{
    dma_clear_interrupt_flags(ADC_DMA, ADC_DMA_CHANNEL, DMA_TCIF | DMA_HTIF | DMA_TEIF );
    adc_start_conversion_regular(ADC_NUM);
}

int inputs_isConversionsFinished(void)
{
    return adc_eos(ADC_NUM) && dma_get_interrupt_flag(ADC_DMA, ADC_DMA_CHANNEL, DMA_TCIF);
}

uint16_t inputs_getValue(enum inputs_enum input)
{
    return conv_results[input];
}
