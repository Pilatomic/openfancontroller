
#include <stdlib.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>

#include "usb.h"

#include "fans.h"
#include "aux.h"

#define EP_ADDR 0x81

/* Define this to include the DFU APP interface. */
#define INCLUDE_DFU_INTERFACE

#ifdef INCLUDE_DFU_INTERFACE
#include <libopencm3/cm3/scb.h>
#include <libopencm3/usb/dfu.h>
#endif

static usbd_device *usbd_dev;

const struct usb_device_descriptor dev_descr = {
    .bLength = USB_DT_DEVICE_SIZE,
    .bDescriptorType = USB_DT_DEVICE,
    .bcdUSB = 0x0200,
    .bDeviceClass = 0,
    .bDeviceSubClass = 0,
    .bDeviceProtocol = 0,
    .bMaxPacketSize0 = 64,
    .idVendor = 0x0483,
    .idProduct = 0x5710,
    .bcdDevice = 0x0200,
    .iManufacturer = 1,
    .iProduct = 2,
    .iSerialNumber = 3,
    .bNumConfigurations = 1,
};

static const uint8_t hid_report_descriptor[] = {
    0x05, 0x0C,        // Usage Page (Consumer)
    0x0A, 0x01, 0x01,  // Usage (Fan Speed)
    0xA1, 0x01,        // Collection (Application)
    0x85, 0x01,        // Report ID (1)
    0x15, 0x00,        // Logical Minimum (0)
    0x25, 0xFF,        // Logical Maximum (255)
    0x35, 0x00,        // Physical Minimum (0)
    0x45, 0xFF,        // Physical Maximum (255)
    0x75, 0x10,        // Report Size (8)
    0x95, 0x04,        // Report Count (4)
    0x81, 0x22,        // Input (Data,Var,Abs,No Wrap,Linear,No Preferred State,No Null Position)
    0x91, 0x22,        // Output (Data,Var,Abs,No Wrap,Linear,No Preferred State,No Null Position)
    0xC0,              // End Collection
    0xA1, 0x01,        // Collection (Application)
    0x85, 0x02,        // Report ID (2)
    0x15, 0x00,        // Logical Minimum (0)
    0x27, 0xFF, 0xFF, 0x00, 0x00,  // Logical Maximum (65534)
    0x35, 0x00,        // Physical Minimum (0)
    0x47, 0xFF, 0xFF, 0x00, 0x00,  // Physical Maximum (65534)
    0x75, 0x10,        // Report Size (16)
    0x95, 0x04,        // Report Count (4)
    0x81, 0x22,        // Input (Data,Var,Abs,No Wrap,Linear,No Preferred State,No Null Position)
    0xC0,              // End Collection
    0xA1, 0x01,        // Collection (Application)
    0x85, 0x03,        // Report ID (3)
    0x15, 0x00,        // Logical Minimum (0)
    0x27, 0xFF, 0xFF, 0x00, 0x00,  // Logical Maximum (65534)
    0x35, 0x00,        // Physical Minimum (0)
    0x47, 0xFF, 0xFF, 0x00, 0x00,  // Physical Maximum (65534)
    0x75, 0x10,        // Report Size (16)
    0x95, 0x04,        // Report Count (4)
    0x81, 0x22,        // Input (Data,Var,Abs,No Wrap,Linear,No Preferred State,No Null Position)
    0xC0,              // End Collection
    0xA1, 0x01,        // Collection (Application)
    0x85, 0x03,        // Report ID (4)
    0x15, 0x00,        // Logical Minimum (0)
    0x25, 0xFF,        // Logical Maximum (255)
    0x35, 0x00,        // Physical Minimum (0)
    0x45, 0xFF,        // Physical Maximum (255)
    0x75, 0x10,        // Report Size (8)
    0x95, 0x04,        // Report Count (9)
    0x91, 0x22,        // Output (Data,Var,Abs,No Wrap,Linear,No Preferred State,No Null Position)
    0xC0,              // End Collection
};


static const struct {
    struct usb_hid_descriptor hid_descriptor;
    struct {
        uint8_t bReportDescriptorType;
        uint16_t wDescriptorLength;
    } __attribute__((packed)) hid_report;
} __attribute__((packed)) hid_function = {
    .hid_descriptor = {
        .bLength = sizeof(hid_function),
        .bDescriptorType = USB_HID_DT_HID,
        .bcdHID = 0x0100,
        .bCountryCode = 0,
        .bNumDescriptors = 1,
    },
    .hid_report = {
        .bReportDescriptorType = USB_HID_DT_REPORT,
        .wDescriptorLength = sizeof(hid_report_descriptor),
    }
};

const struct usb_endpoint_descriptor hid_endpoint = {
    .bLength = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType = USB_DT_ENDPOINT,
    .bEndpointAddress = EP_ADDR,
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
    .wMaxPacketSize = 16,
    .bInterval = 0x20,
};

const struct usb_interface_descriptor hid_iface = {
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 0,
    .bAlternateSetting = 0,
    .bNumEndpoints = 1,
    .bInterfaceClass = USB_CLASS_HID,
    .bInterfaceSubClass = 0,
    .bInterfaceProtocol = 0,
    .iInterface = 0,

    .endpoint = &hid_endpoint,

    .extra = &hid_function,
    .extralen = sizeof(hid_function),
};

#ifdef INCLUDE_DFU_INTERFACE
const struct usb_dfu_descriptor dfu_function = {
    .bLength = sizeof(struct usb_dfu_descriptor),
    .bDescriptorType = DFU_FUNCTIONAL,
    .bmAttributes = USB_DFU_CAN_DOWNLOAD | USB_DFU_WILL_DETACH,
    .wDetachTimeout = 255,
    .wTransferSize = 1024,
    .bcdDFUVersion = 0x011A,
};

const struct usb_interface_descriptor dfu_iface = {
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 1,
    .bAlternateSetting = 0,
    .bNumEndpoints = 0,
    .bInterfaceClass = 0xFE,
    .bInterfaceSubClass = 1,
    .bInterfaceProtocol = 1,
    .iInterface = 0,

    .extra = &dfu_function,
    .extralen = sizeof(dfu_function),
};
#endif

const struct usb_interface ifaces[] = {{
    .num_altsetting = 1,
    .altsetting = &hid_iface,
#ifdef INCLUDE_DFU_INTERFACE
}, {
    .num_altsetting = 1,
    .altsetting = &dfu_iface,
#endif
}};

const struct usb_config_descriptor config = {
    .bLength = USB_DT_CONFIGURATION_SIZE,
    .bDescriptorType = USB_DT_CONFIGURATION,
    .wTotalLength = 0,
#ifdef INCLUDE_DFU_INTERFACE
    .bNumInterfaces = 2,
#else
    .bNumInterfaces = 1,
#endif
    .bConfigurationValue = 1,
    .iConfiguration = 0,
    .bmAttributes = 0xC0,
    .bMaxPower = 0x32,

    .interface = ifaces,
};

static const char *usb_strings[] = {
    "BlackMesa Corp.",
    "Open Fan Controller",
    "1",
};

/* Buffer to be used for control requests. */
uint8_t usbd_control_buffer[128];

static int usb_enable_report_transmission = 0;

static enum usbd_request_return_codes standard_control_request(usbd_device *dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
                                                          void (**complete)(usbd_device *, struct usb_setup_data *))
{
    (void)complete;
    (void)dev;

    if((req->bmRequestType == (USB_REQ_TYPE_IN | USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE)) &&
            (req->bRequest == USB_REQ_GET_DESCRIPTOR) &&
            (req->wValue == 0x2200)) // Report descriptor requested
    {
        /* Handle the HID report descriptor. */
        *buf = (uint8_t *)hid_report_descriptor;
        *len = sizeof(hid_report_descriptor);

        return USBD_REQ_HANDLED;
    }

    return USBD_REQ_NOTSUPP;
}

#ifdef INCLUDE_DFU_INTERFACE
static void dfu_detach_complete(usbd_device *dev, struct usb_setup_data *req)
{
    (void)req;
    (void)dev;

    // Set BOOT0 pin to HIGH
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO8);
    gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO8);
    gpio_set(GPIOB, GPIO8);

    // Ugly delay to let BOOT0 external capacitor charge, roughly 1.5ms
    for(volatile uint32_t i = 0; i < 5000 ; i++);

    scb_reset_system();
}

#endif

static enum usbd_request_return_codes class_control_request(usbd_device *dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
            void (**complete)(usbd_device *, struct usb_setup_data *))
{
    (void)dev;
    // HID Set Report
    if((req->bmRequestType == (USB_REQ_TYPE_OUT | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE)) &&
            (req->bRequest == USB_HID_REQ_TYPE_SET_REPORT) &&
            ((req->wValue >> 8) == USB_HID_REPORT_TYPE_OUTPUT))
    {
        const uint8_t reportId = req->wValue & 0xff;

        // Report ID 1 : Fan PWM
        if((reportId == 1) && (*len == (FANS_COUNT+1)))
        {
            uint8_t *pwmVal = &(*buf)[1];
            for(int i = 0 ; i < FANS_COUNT ; i++)
            {
                fans_setFanPwm(i, *pwmVal++);
            }
            return USBD_REQ_HANDLED;
        }

        // Report ID 4 : Led Color
        if((reportId == 4) && (*len == 10))
        {
            aux_ws2812_write(0, 9 ,(*buf) + 1);
            return USBD_REQ_HANDLED;
        }
    }

#ifdef INCLUDE_DFU_INTERFACE
    // DFU Detach
    else if ((req->bmRequestType == (USB_REQ_TYPE_OUT | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE)) &&
             (req->bRequest == DFU_DETACH))
    {
        *complete = dfu_detach_complete;
        return USBD_REQ_HANDLED;
    }
#endif

    return USBD_REQ_NOTSUPP;
}


static void hid_set_config(usbd_device *dev, uint16_t wValue)
{
    (void)wValue;
    (void)dev;

    usbd_ep_setup(dev, EP_ADDR, USB_ENDPOINT_ATTR_INTERRUPT, 16, NULL);

    usbd_register_control_callback(
                dev,
                USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
                USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
                standard_control_request);
    usbd_register_control_callback(
                dev,
                USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
                USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
                class_control_request);

    //Enable report transmission here, since attempting to write to EP before this stage hangs
    usb_enable_report_transmission = 1;

}

void usb_setup(void)
{
    usb_enable_report_transmission = 0;
    usbd_dev = usbd_init(&st_usbfs_v2_usb_driver, &dev_descr, &config,
            usb_strings, sizeof(usb_strings) / sizeof(usb_strings[0]),
            usbd_control_buffer, sizeof(usbd_control_buffer));
    usbd_register_set_config_callback(usbd_dev, hid_set_config);
}

void usb_poll(void)
{
    usbd_poll(usbd_dev);
}


#warning DEBUG ONLY
volatile int errorCount = 0;

void usb_send_report(char* buf, int length)
{
    if(usb_enable_report_transmission)
    {
        if( 0 == usbd_ep_write_packet(usbd_dev, EP_ADDR, buf, length))
            errorCount++;
    }
}
