#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/crs.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/syscfg.h>
#include "usb.h"
#include "fans.h"
#include "aux.h"
#include "inputs.h"

typedef struct RgbColor
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
} RgbColor;

typedef struct HsvColor
{
    unsigned char h;
    unsigned char s;
    unsigned char v;
} HsvColor;

RgbColor HsvToRgb(HsvColor hsv)
{
    RgbColor rgb;
    unsigned char region, remainder, p, q, t;

    if (hsv.s == 0)
    {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb;
    }

    region = hsv.h / 43;
    remainder = (hsv.h - (region * 43)) * 6;

    p = (hsv.v * (255 - hsv.s)) >> 8;
    q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
    t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
        case 0:
            rgb.r = hsv.v; rgb.g = t; rgb.b = p;
            break;
        case 1:
            rgb.r = q; rgb.g = hsv.v; rgb.b = p;
            break;
        case 2:
            rgb.r = p; rgb.g = hsv.v; rgb.b = t;
            break;
        case 3:
            rgb.r = p; rgb.g = q; rgb.b = hsv.v;
            break;
        case 4:
            rgb.r = t; rgb.g = p; rgb.b = hsv.v;
            break;
        default:
            rgb.r = hsv.v; rgb.g = p; rgb.b = q;
            break;
    }

    return rgb;
}

static uint32_t currMs = 0;
/* Called when systick fires */
void sys_tick_handler(void)
{
    currMs++;
    //gpio_toggle(GPIOA, GPIO15);
}

/*
 * Set up timer to fire every x milliseconds
 * This is a unusual usage of systick, be very careful with the 24bit range
 * of the systick counter!  You can range from 1 to 2796ms with this.
 */
static void systick_setup(int xms)
{
    /* div8 per ST, stays compatible with M3/M4 parts, well done ST */
    systick_set_clocksource(STK_CSR_CLKSOURCE_EXT);
    /* clear counter so it starts right away */
    STK_CVR = 0;

    systick_set_reload(rcc_ahb_frequency / 8 / 1000 * xms);
    systick_counter_enable();
    systick_interrupt_enable();
}

static void mco_setup(void)
{
    /* PA8 to AF 0 for MCO */
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO8);
    gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, GPIO8);
    gpio_set_af(GPIOA, 0, GPIO8);

    /* clock output on pin PA8 (allows checking with scope) */
    rcc_set_mco(RCC_CFGR_MCO_SYSCLK);
}

int main(void)
{
    // 48MHz internal clock with auto trim from USB sync frame
    rcc_clock_setup_in_hsi48_out_48mhz();
    crs_autotrim_usb_enable();
    rcc_set_usbclk_source(RCC_HSI48);

    /* Setup Status LED Gpio*/
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO15);
    gpio_clear(GPIOA, GPIO15);

    //mco_setup();
    usb_setup();
    fans_setup();
    inputs_setup();
    aux_ws2812_setup();

    systick_setup(1); // in ms

    fans_startRpmCount();
    inputs_startConversions();

    while (1)
    {
        usb_poll();

        if(fans_isRpmCountFinished())
        {
            uint8_t buffer[1 + FANS_COUNT * 2];
            buffer[0] = 2; // Report ID
            uint8_t *bufPtr = &buffer[1];
            for(int i = 0 ; i < FANS_COUNT; i++)
            {
                uint16_t hidRpm = fans_getRpmCountForFan(i);
                *bufPtr++ = hidRpm & 0xff;
                *bufPtr++ = (hidRpm >> 8) & 0xff;
            }
            usb_send_report((char*)buffer, sizeof(buffer));
            gpio_toggle(GPIOA, GPIO15);  // Status Led
            fans_startRpmCount();
        }

        // We need some timer here
        static uint32_t prevMs = 0;
        if(inputs_isConversionsFinished() && ((currMs - prevMs) > 100))
        {
            uint8_t buffer[1 + INPUTS_COUNT * 2];
            buffer[0] = 3; // Report ID
            uint8_t *bufPtr = &buffer[1];
            for(int i = 0 ; i < INPUTS_COUNT; i++)
            {
                uint16_t hidVal = inputs_getValue(i);
                *bufPtr++ = hidVal & 0xff;
                *bufPtr++ = (hidVal >> 8) & 0xff;
            }
            usb_send_report((char*)buffer, sizeof(buffer));
            inputs_startConversions();
            prevMs = currMs;
        }
    }
}

