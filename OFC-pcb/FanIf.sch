EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Fan_4pin M?
U 1 1 5EB9708A
P 5350 3450
AR Path="/5EB9708A" Ref="M?"  Part="1" 
AR Path="/5EB93B3B/5EB9708A" Ref="M1"  Part="1" 
AR Path="/5EBB4948/5EB9708A" Ref="M2"  Part="1" 
AR Path="/5EBB535E/5EB9708A" Ref="M?"  Part="1" 
AR Path="/5EBB5555/5EB9708A" Ref="M3"  Part="1" 
AR Path="/5EBB555B/5EB9708A" Ref="M4"  Part="1" 
F 0 "M4" H 5508 3546 50  0000 L CNN
F 1 "Fan_4pin" H 5508 3455 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x04_P2.54mm_Vertical" H 5350 3460 50  0001 C CNN
F 3 "http://www.formfactors.org/developer%5Cspecs%5Crev1_2_public.pdf" H 5350 3460 50  0001 C CNN
F 4 " 538-47053-1000 " H 5350 3450 50  0001 C CNN "Mouser Ref"
	1    5350 3450
	1    0    0    -1  
$EndComp
Text HLabel 4300 2950 0    50   Input ~ 0
12V
Text HLabel 4300 4150 0    50   Input ~ 0
GND
Text HLabel 4300 3100 0    50   Input ~ 0
TACH
Text HLabel 4300 3850 0    50   Input ~ 0
PWM
$Comp
L Device:R R1
U 1 1 5EB98853
P 4850 3150
AR Path="/5EB93B3B/5EB98853" Ref="R1"  Part="1" 
AR Path="/5EBB4948/5EB98853" Ref="R2"  Part="1" 
AR Path="/5EBB535E/5EB98853" Ref="R?"  Part="1" 
AR Path="/5EBB5555/5EB98853" Ref="R3"  Part="1" 
AR Path="/5EBB555B/5EB98853" Ref="R4"  Part="1" 
F 0 "R4" H 4920 3196 50  0000 L CNN
F 1 "10k" H 4920 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4780 3150 50  0001 C CNN
F 3 "~" H 4850 3150 50  0001 C CNN
F 4 "71-CRCW080510K0JNEAC" H 4850 3150 50  0001 C CNN "Mouser Ref"
	1    4850 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2950 5350 3150
Wire Wire Line
	5350 4150 5350 3650
Wire Wire Line
	4300 3100 4400 3100
Wire Wire Line
	4400 3100 4400 3150
Wire Wire Line
	4300 4150 4400 4150
Connection ~ 4400 4150
Wire Wire Line
	4400 3550 4400 4150
Wire Wire Line
	4400 4150 4850 4150
Wire Wire Line
	4850 4050 4850 4150
Connection ~ 4850 4150
Wire Wire Line
	4850 4150 5350 4150
Wire Wire Line
	4850 3550 4850 3650
Wire Wire Line
	4850 3550 5050 3550
Wire Wire Line
	4850 3300 4850 3350
Connection ~ 4850 3350
Wire Wire Line
	4850 3350 5050 3350
Wire Wire Line
	4850 3000 4850 2950
Wire Wire Line
	4850 2950 5350 2950
Wire Wire Line
	4700 2950 4850 2950
Connection ~ 4850 2950
Wire Wire Line
	4400 2950 4300 2950
$Comp
L Device:Fuse F1
U 1 1 5EBD1939
P 4550 2950
AR Path="/5EB93B3B/5EBD1939" Ref="F1"  Part="1" 
AR Path="/5EBB4948/5EBD1939" Ref="F2"  Part="1" 
AR Path="/5EBB5555/5EBD1939" Ref="F3"  Part="1" 
AR Path="/5EBB555B/5EBD1939" Ref="F4"  Part="1" 
F 0 "F4" V 4353 2950 50  0000 C CNN
F 1 "F2A" V 4444 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4480 2950 50  0001 C CNN
F 3 "~" H 4550 2950 50  0001 C CNN
F 4 "594-MFU0805-FF-2.0" V 4550 2950 50  0001 C CNN "Mouser Ref"
	1    4550 2950
	0    1    1    0   
$EndComp
Text Label 4950 2950 0    50   ~ 0
FAN_POWER
$Comp
L Transistor_BJT:DTC143E QB1
U 1 1 5EBEEE71
P 4750 3850
AR Path="/5EB93B3B/5EBEEE71" Ref="QB1"  Part="1" 
AR Path="/5EBB4948/5EBEEE71" Ref="QB2"  Part="1" 
AR Path="/5EBB5555/5EBEEE71" Ref="QB3"  Part="1" 
AR Path="/5EBB555B/5EBEEE71" Ref="QB4"  Part="1" 
F 0 "QB4" H 4938 3896 50  0000 L CNN
F 1 "MMUN2232LT1G" H 4938 3805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4750 3850 50  0001 L CNN
F 3 "" H 4750 3850 50  0001 L CNN
F 4 " 863-MMUN2232LT1G " H 4750 3850 50  0001 C CNN "Mouser Ref"
	1    4750 3850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:DTC143E QA1
U 1 1 5EBF0A5F
P 4500 3350
AR Path="/5EB93B3B/5EBF0A5F" Ref="QA1"  Part="1" 
AR Path="/5EBB4948/5EBF0A5F" Ref="QA2"  Part="1" 
AR Path="/5EBB5555/5EBF0A5F" Ref="QA3"  Part="1" 
AR Path="/5EBB555B/5EBF0A5F" Ref="QA4"  Part="1" 
F 0 "QA4" H 4688 3396 50  0000 L CNN
F 1 "MMUN2232LT1G" H 4688 3305 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4500 3350 50  0001 L CNN
F 3 "" H 4500 3350 50  0001 L CNN
F 4 " 863-MMUN2232LT1G " H 4500 3350 50  0001 C CNN "Mouser Ref"
	1    4500 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4750 3350 4850 3350
Wire Wire Line
	4300 3850 4500 3850
$EndSCHEMATC
