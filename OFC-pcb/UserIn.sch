EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5EBC545B
P 3750 3250
AR Path="/5EBC48CA/5EBC545B" Ref="J3"  Part="1" 
AR Path="/5EBCB1B0/5EBC545B" Ref="J4"  Part="1" 
AR Path="/5EBCD055/5EBC545B" Ref="J?"  Part="1" 
AR Path="/5EBCD05A/5EBC545B" Ref="J?"  Part="1" 
AR Path="/5EBD0BB0/5EBC545B" Ref="J5"  Part="1" 
AR Path="/5EBD0BB5/5EBC545B" Ref="J6"  Part="1" 
F 0 "J6" H 3668 2925 50  0000 C CNN
F 1 "Conn_01x02" H 3668 3016 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 3750 3250 50  0001 C CNN
F 3 "~" H 3750 3250 50  0001 C CNN
F 4 "538-22-11-2022" H 3750 3250 50  0001 C CNN "Mouser Ref"
	1    3750 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5EBC5461
P 5100 2850
AR Path="/5EBC48CA/5EBC5461" Ref="R6"  Part="1" 
AR Path="/5EBCB1B0/5EBC5461" Ref="R8"  Part="1" 
AR Path="/5EBCD055/5EBC5461" Ref="R?"  Part="1" 
AR Path="/5EBCD05A/5EBC5461" Ref="R?"  Part="1" 
AR Path="/5EBD0BB0/5EBC5461" Ref="R10"  Part="1" 
AR Path="/5EBD0BB5/5EBC5461" Ref="R12"  Part="1" 
F 0 "R12" V 4893 2850 50  0000 C CNN
F 1 "10k" V 4984 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 2850 50  0001 C CNN
F 3 "~" H 5100 2850 50  0001 C CNN
F 4 "71-CRCW080510K0JNEAC" H 5100 2850 50  0001 C CNN "Mouser Ref"
	1    5100 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5EBC5476
P 4250 3150
AR Path="/5EBC48CA/5EBC5476" Ref="R5"  Part="1" 
AR Path="/5EBCB1B0/5EBC5476" Ref="R7"  Part="1" 
AR Path="/5EBCD055/5EBC5476" Ref="R?"  Part="1" 
AR Path="/5EBCD05A/5EBC5476" Ref="R?"  Part="1" 
AR Path="/5EBD0BB0/5EBC5476" Ref="R9"  Part="1" 
AR Path="/5EBD0BB5/5EBC5476" Ref="R11"  Part="1" 
F 0 "R11" V 4043 3150 50  0000 C CNN
F 1 "1k" V 4134 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4180 3150 50  0001 C CNN
F 3 "~" H 4250 3150 50  0001 C CNN
F 4 "71-CRCW08051K00FKEAC" H 4250 3150 50  0001 C CNN "Mouser Ref"
	1    4250 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 3150 4050 3150
$Comp
L Device:C C1
U 1 1 5EBC547D
P 4650 3400
AR Path="/5EBC48CA/5EBC547D" Ref="C1"  Part="1" 
AR Path="/5EBCB1B0/5EBC547D" Ref="C2"  Part="1" 
AR Path="/5EBCD055/5EBC547D" Ref="C?"  Part="1" 
AR Path="/5EBCD05A/5EBC547D" Ref="C?"  Part="1" 
AR Path="/5EBD0BB0/5EBC547D" Ref="C3"  Part="1" 
AR Path="/5EBD0BB5/5EBC547D" Ref="C4"  Part="1" 
F 0 "C4" H 4765 3446 50  0000 L CNN
F 1 "100n" H 4765 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4688 3250 50  0001 C CNN
F 3 "~" H 4650 3400 50  0001 C CNN
F 4 "80-C0805C104M5R" H 4650 3400 50  0001 C CNN "Mouser Ref"
	1    4650 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3250 4650 3150
Connection ~ 4650 3150
Wire Wire Line
	4650 3150 4400 3150
Wire Wire Line
	4650 3650 4650 3550
Text HLabel 5500 3150 2    50   Input ~ 0
SIG
Wire Wire Line
	5500 3150 4650 3150
Text HLabel 5500 2850 2    50   Input ~ 0
VCC
Wire Wire Line
	5250 2850 5500 2850
Wire Wire Line
	3950 3250 4100 3250
Wire Wire Line
	4100 3250 4100 3650
Wire Wire Line
	4100 3650 4650 3650
Wire Wire Line
	4650 3650 5500 3650
Connection ~ 4650 3650
Text HLabel 5500 3650 2    50   Input ~ 0
GND
Wire Wire Line
	4050 2850 4050 3150
Wire Wire Line
	4050 2850 4950 2850
Connection ~ 4050 3150
Wire Wire Line
	4050 3150 3950 3150
$EndSCHEMATC
